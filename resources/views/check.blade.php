<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>大学生经济独立意向与情况调查</title>
<style>
table{width:100%;empty-cells:show;background-color:transparent;border-collapse:collapse;border-spacing:0}
table th{text-align:left; font-weight:400}
.table thead th{background-color:#F5FAFE}
.table-striped tbody > tr:nth-child(odd) > td,
.table-striped tbody > tr:nth-child(odd) > th{background-color:#f9f9f9}
.table-hover tbody tr:hover td,
.table-hover tbody tr:hover th{background-color: #f5f5f5}

.head  { height:auto;text-align:center;}
td	   {	text-align: center;
    		width: 100px;
   			border-top: 2px solid black;
    		border-bottom: 2px solid black;
    		border-left: 1px solid black;
    		border-right: 1px solid black;
		}
.table th{ height:40px;
			text-align:center;
		 }	
.table td, .table th { padding:3px 7px 2px 7px;}
.table { border:1px solid black;
    	 width: 100%;
    	 max-width: 100%;
    	 margin-bottom: 20px;
		 border-collapse:collapse;
		 height:400px;
		}
</style>
</head>
<body class="back">
<div class="head">
	<p style="font-size:36px">大学生经济独立意向与情况调查</p>
</div>
<div>
	 <table class="table table-bordered table-striped table-condensed">
	 	<thead >
			<tr>
				<th><b>#</b></th>
				<th><b>年龄</b></th>
				<th><b>1</b></th>
				<th><b>2</b></th>
				<th><b>3</b></th>
				<th><b>4</b></th>
				<th><b>5</b></th>
				<th><b>6</b></th>
				<th><b>7</b></th>
				<th><b>8</b></th>
				<th><b>9</b></th>
				<th><b>10</b></th>
			</tr>
		</thead>
		<tbody>
        <?php $counter = 1 ?>
        <?php $len = 0 ?>
        <?php $age = 0 ?>
        <?php $num2 = 0 ?>
        <?php $num3 = 0 ?>
        <?php $num4 = 0 ?>
        <?php $num5 = 0 ?>
        <?php $num6 = 0 ?>
        <?php $num7 = 0 ?>
        <?php $num8 = 0 ?>
        <?php $num9 = 0 ?>
        <?php $num1_1 = 0 ?>
        <?php $num1_2 = 0 ?>
        <?php $num1_3 = 0 ?>
        <?php $num1_4 = 0 ?>
        <?php $num1_5 = 0 ?>
        <?php $num10_1 = 0 ?>
        <?php $num10_2 = 0 ?>
        <?php $num10_3 = 0 ?>
        <?php $num10_4 = 0 ?>
        <?php $num10_5 = 0 ?>
		@foreach ($data as $row)
			<?php $age += $row->age ?>
            <?php $num2 += $row->num2 ?>
            <?php $num3 += $row->num3 ?>
            <?php $num4 += $row->num4 ?>
            <?php $num5 += $row->num5 ?>
            <?php $num6 += $row->num6 ?>
            <?php $num7 += $row->num7 ?>
            <?php $num8 += $row->num8 ?>
            <?php $num9 += $row->num9 ?>
            <?php $num1_1 += $row->num1_1 ?>
            <?php $num1_2 += $row->num1_2 ?>
            <?php $num1_3 += $row->num1_3 ?>
            <?php $num1_4 += $row->num1_4 ?>
            <?php $num1_5 += $row->num1_5 ?>
            <?php $num10_1 += $row->num10_1 ?>
            <?php $num10_2 += $row->num10_2 ?>
            <?php $num10_3+= $row->num10_3 ?>
            <?php $num10_4+= $row->num10_4 ?>
            <?php $num10_5 += $row->num10_5 ?>
            <?php $len += 1 ?>
		@endforeach
		<tr>
			<th>total/aver</th>
			<th><?php if($len)$age/$len ?></th>
			<th><?php $num1_1 ?></th>
			<th><?php $num1_2 ?></th>
			<th> <?php $num1_3 ?></th>
			<th><?php $num1_4  ?></th>
			<th><?php $num1_5 ?></th>
			<th><?php $num2 ?></th>
			<th><?php $num3 ?></th>
			<th><?php $num4 ?></th>
			<th><?php $num5 ?></th>
			<th><?php $num6 ?></th>
			<th><?php $num7 ?></th>
			<th><?php $num8 ?></th>
			<th><?php $num9 ?></th>
			<th><?php $num10_1 ?></th>
			<th><?php $num10_2 ?></th>
			<th> <?php $num10_3 ?></th>
			<th><?php $num10_4  ?></th>
			<th><?php $num10_5 ?></th>

		</tr>
		@foreach ($data as $row)
			<tr name="{{ $counter }}">
				<th class="include_id">{{ $row->id }}</th>
				<th >{{ $row->age }}</th>
				<th >{{$row->num1_1}}{{$row->num1_2}}{{$row->num1_3}}{{$row->num1_4}}{{$row->num1_5}}</th>
				<th>{{$row->num2}}</th>
				<th>{{$row->num3}}</th>
				<th>{{$row->num4}}</th>
				<th>{{$row->num5}}</th>
				<th>{{$row->num6}}</th>
				<th>{{$row->num7}}</th>
				<th>{{$row->num8}}</th>
				<th>{{$row->num9}}</th>
				<th >{{$row->num10_1}}{{$row->num10_2}}{{$row->num10_3}}{{$row->num10_4}}{{$row->num10_5}}</th>
                <?php $counter += 1 ?>
			</tr>

		@endforeach
		</tbody>
	 </table>
</div>
</body>
</html>

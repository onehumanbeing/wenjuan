<!DOCTYPE html>
<html lang  ="zh-cn">
<head>
	<meta charset    ="utf-8">
	<meta http-equiv ="X-UA-Compatible" content="IE=edge">
	<meta name       ="viewport" content="width=device-width, initial-scale=1">
	<title>大学生经济独立意向与情况调查</title>
    <link href       ="weui.min.css" rel="stylesheet">
	<link href       ="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href       ="css/sweetalert.css" rel="stylesheet">
    <script src="dist/sweetalert-dev.js"></script>
    <link rel="stylesheet" href="dist/sweetalert.css">

    <script type="text/javascript"  scr="js/jquery.min.js"></script>
    <script type="text/javascript"  scr="js/sweetalert.min.js"></script>
</head>
<div>
<div style="margin-left: 20px;margin-right:20px;">
<div class="page__hd">
        <h1 class="page__title" style="text-align: center">大学生经济独立意向与情况调查</h1>
   </div>

 <div class="row">
 <div class="col-md-1"></div>
 <div class="col-md-10">
  <!--第1题-->
 <div class="weui-cells weui-cells_checkbox">
 <label style="font-size: 20px">1.你觉得大学生可以以何种方式逐渐达到经济独立？（多选）</label><br />
     <div class="weui-cells weui-cells_form">
         <div class="weui-cell">
             <div class="weui-cell__hd"><label class="weui-label">年龄</label></div>
             <div class="weui-cell__bd">
                 <input class="weui-input" type="number" name="age" id="age" pattern="[0-9]*" placeholder="请输入年龄">
             </div>
         </div>
     </div>

         <label class="weui-cell weui-check__label" for="num1_1">
                <div class="weui-cell__hd">
                    <input type="checkbox" class="weui-check" name="num1_1" id="num1_1" value="1">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>A.奖学金</p>
                </div>
            </label>
            <label class="weui-cell weui-check__label" for="num1_2">
                <div class="weui-cell__hd">
                    <input type="checkbox" name="num1_2" class="weui-check" id="num1_2" value="1">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>B.兼职本专业工作</p>
                </div>
            </label>
      
            <label class="weui-cell weui-check__label" for="num1_3">
                <div class="weui-cell__hd">
                    <input type="checkbox" name="num1_3" class="weui-check" id="num1_3" value="1">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>C.打工</p>
                </div>
            </label>
     
            <label class="weui-cell weui-check__label" for="num1_4">
                <div class="weui-cell__hd">
                    <input type="checkbox" name="num1_4" class="weui-check" id="num1_4" value="1">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>D.在校创业</p>
                </div>
            </label>
       
            <label class="weui-cell weui-check__label" for="num1_5">
                <div class="weui-cell__hd">
                    <input type="checkbox" name="num1_5" class="weui-check" id="num1_5" value="1">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>E.休学工作</p>
                </div>
            </label>
        </div>
<!--第2题-->
<div class="weui-cells weui-cells_checkbox">
  <label style="font-size: 20px">2.你觉得成年人应该最晚在何时达到经济独立？</label><br />
            <label class="weui-cell weui-check__label" for="num2_1">
                <div class="weui-cell__hd">
                    <input type="radio" class="weui-check" name="num2" id="num2_1" value="1">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>A.本科期间</p>
                </div>
            </label>
            <label class="weui-cell weui-check__label" for="num2_2">
                <div class="weui-cell__hd">
                    <input type="radio" name="num2" class="weui-check" id="num2_2" value="2">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>B.研究生期间</p>
                </div>
            </label>
           
            <label class="weui-cell weui-check__label" for="num2_3">
                <div class="weui-cell__hd">
                    <input type="radio" name="num2" class="weui-check" id="num2_3" value="3">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>C.工作以后</p>
                </div>
            </label>
       
            <label class="weui-cell weui-check__label" for="num2_4">
                <div class="weui-cell__hd">
                    <input type="radio" name="num2" class="weui-check" id="num2_4" value="4">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>D.结婚以后</p>
                </div>
            </label>
         
            <label class="weui-cell weui-check__label" for="num2_5">
                <div class="weui-cell__hd">
                    <input type="radio" name="num2" class="weui-check" id="num2_5" value="5">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>E.说不好</p>
                </div>
            </label>
        </div>
<!--第3题-->
<div class="weui-cells weui-cells_checkbox">
  <label style="font-size: 20px">3.你认为要达到经济独立，大学生需要多少的月收入？</label><br />
            <label class="weui-cell weui-check__label" for="num3_1">
                <div class="weui-cell__hd">
                    <input type="radio" class="weui-check" name="num3" id="num3_1" value="1">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>A.500---1000</p>
                </div>
            </label>
            <label class="weui-cell weui-check__label" for="num3_2">
                <div class="weui-cell__hd">
                    <input type="radio" name="num3" class="weui-check" id="num3_2" value="2">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>B.1000---3000</p>
                </div>
            </label>
           
            <label class="weui-cell weui-check__label" for="num3_3">
                <div class="weui-cell__hd">
                    <input type="radio" name="num3" class="weui-check" id="num3_3" value="3">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>C.3000---5000</p>
                </div>
            </label>
       
            <label class="weui-cell weui-check__label" for="num3_4" >
                <div class="weui-cell__hd">
                    <input type="radio" name="num3" class="weui-check" id="num3_4" value="4">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>D.5000---10000</p>
                </div>
            </label>
         
            <label class="weui-cell weui-check__label" for="num3_5">
                <div class="weui-cell__hd">
                    <input type="radio" name="num3" class="weui-check" id="num3_5" value="5">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>E.10000以上</p>
                </div>
            </label>
        </div>
<!--第4题-->
<div class="weui-cells weui-cells_checkbox">
  <label style="font-size: 20px">4.你认为经济独立的首要先决条件是什么？</label><br />
            <label class="weui-cell weui-check__label" for="num4_1">
                <div class="weui-cell__hd">
                    <input type="radio" class="weui-check" name="num4" id="num4_1" value="1">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>A.自身经济收入</p>
                </div>
            </label>
            <label class="weui-cell weui-check__label" for="num4_2">
                <div class="weui-cell__hd">
                    <input type="radio" name="num4" class="weui-check" id="num4_2" value="2">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>B.自身财务管理能力</p>
                </div>
            </label>
           
            <label class="weui-cell weui-check__label" for="num4_3">
                <div class="weui-cell__hd">
                    <input type="radio" name="num4" class="weui-check" id="num4_3" value="3">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>C.父母支持</p>
                </div>
            </label>
       
            <label class="weui-cell weui-check__label" for="num4_4">
                <div class="weui-cell__hd">
                    <input type="radio" name="num4" class="weui-check" id="num4_4" value="4">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>D.其他</p>
                </div>
            </label>
        </div>




<!--第5题-->
<div class="weui-cells weui-cells_checkbox">
  <label style="font-size: 20px">5.你认为经济独立将会给你带来的最大的变化在哪方面？</label><br />
            <label class="weui-cell weui-check__label" for="num5_1">
                <div class="weui-cell__hd">
                    <input type="radio" class="weui-check" name="num5" id="num5_1" value="1">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>A.工作</p>
                </div>
            </label>
            <label class="weui-cell weui-check__label" for="num5_2">
                <div class="weui-cell__hd">
                    <input type="radio" name="num5" class="weui-check" id="num5_2" value="2">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>B.自身生活能力</p>
                </div>
            </label>
           
            <label class="weui-cell weui-check__label" for="num5_3">
                <div class="weui-cell__hd">
                    <input type="radio" name="num5" class="weui-check" id="num5_3" value="3">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>C.人际关系</p>
                </div>
            </label>
       
            <label class="weui-cell weui-check__label" for="num5_4">
                <div class="weui-cell__hd">
                    <input type="radio" name="num5" class="weui-check" id="num5_4" value="4">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>D.其他</p>
                </div>
            </label>
        </div>






<!--第6题-->
<div class="weui-cells weui-cells_checkbox">
  <label style="font-size: 20px">6.	你每月消费水平如何？</label><br />
            <label class="weui-cell weui-check__label" for="num6_1">
                <div class="weui-cell__hd">
                    <input type="radio" class="weui-check" name="num6" id="num6_1" value="1" >
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>A. 500~800</p>
                </div>
            </label>
            <label class="weui-cell weui-check__label" for="num6_2">
                <div class="weui-cell__hd">
                    <input type="radio" name="num6" class="weui-check" id="num6_2" value="2">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>B. 800～2000</p>
                </div>
            </label>
           
            <label class="weui-cell weui-check__label" for="num6_3">
                <div class="weui-cell__hd">
                    <input type="radio" name="num6" class="weui-check" id="num6_3" value="3">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>C. 2000以上</p>
                </div>
            </label>
        </div>

<!--第7题-->
<div class="weui-cells weui-cells_checkbox">
  <label style="font-size: 20px">7.	你的生活费来源是？</label><br />
            <label class="weui-cell weui-check__label" for="num7_1">
                <div class="weui-cell__hd">
                    <input type="radio" class="weui-check" name="num7" id="num7_1" value="1">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>A. 完全是父母给的</p>
                </div>
            </label>
            <label class="weui-cell weui-check__label" for="num7_2">
                <div class="weui-cell__hd">
                    <input type="radio" name="num7" class="weui-check" id="num7_2" value="2">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>B. 完全是自己努力得来的（奖学金或打工赚取）</p>
                </div>
            </label>
           
            <label class="weui-cell weui-check__label" for="num7_3">
                <div class="weui-cell__hd">
                    <input type="radio" name="num7" class="weui-check" id="num7_3" value="3">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>C. 一部分父母给的，一部分自己得来的</p>
                </div>
            </label>
        </div>

<!--第8题-->
<div class="weui-cells weui-cells_checkbox">
  <label style="font-size: 20px">8.	有通过自己的劳动赚取自己生活费的打算么？</label><br />
            <label class="weui-cell weui-check__label" for="num8_1">
                <div class="weui-cell__hd">
                    <input type="radio" class="weui-check" name="num8" value="1" id="num8_1" >
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>A. 有，并且已经这样做了</p>
                </div>
            </label>
            <label class="weui-cell weui-check__label" for="num8_2">
                <div class="weui-cell__hd">
                    <input type="radio" name="num8" class="weui-check" value="2" id="num8_2">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>B. 有，但还没有付诸实际</p>
                </div>
            </label>
           
            <label class="weui-cell weui-check__label" for="num8_3">
                <div class="weui-cell__hd">
                    <input type="radio" name="num8" class="weui-check" id="num8_3" value="3">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>C. 完全没有</p>
                </div>
            </label>
        </div>
<!--第9题-->
<div class="weui-cells weui-cells_checkbox">

  <label style="font-size: 20px">9.	你认为大学生活在经济方面完全依赖父母合理吗？</label><br />
            <label class="weui-cell weui-check__label" for="num9_1">
                <div class="weui-cell__hd">
                    <input type="radio" class="weui-check" name="num9" id="num9_1" value="1">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>A. 合理，父母应该在我们上学的时候经济上支持我们</p>
                </div>
            </label>
            <label class="weui-cell weui-check__label" for="num9_2">
                <div class="weui-cell__hd">
                    <input type="radio" name="num9" class="weui-check" id="num9_2" value="2">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>B. 不合理，已经成年了应该有一定的能力，自己生存</p>
                </div>
            </label>
           
            <label class="weui-cell weui-check__label" for="num9_3">
                <div class="weui-cell__hd">
                    <input type="radio" name="num9" class="weui-check" id="num9_3" value="3">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>C. 一半合理一半不合理，视情况而定</p>
                </div>
            </label>      

        </div>
<!--第10题-->
<div class="weui-cells weui-cells_checkbox">
 <!--第1题-->
 <label style="font-size: 20px">10.	你认为个人经济独立会对你有怎样的影响？（多选）</label><br />
            <label class="weui-cell weui-check__label" for="num10_1">
                <div class="weui-cell__hd">
                    <input type="checkbox" class="weui-check" name="num10_1" id="num10_1" value="1" >
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>A. 锻炼个人独立生活能力</p>
                </div>
            </label>
            <label class="weui-cell weui-check__label" for="num10_2">
                <div class="weui-cell__hd">
                    <input type="checkbox" name="num10_2" class="weui-check" id="num10_2" value="1">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>B. 提前接触社会，增长经验</p>
                </div>
            </label>
      
            <label class="weui-cell weui-check__label" for="num10_3">
                <div class="weui-cell__hd">
                    <input type="checkbox" name="num10_3" class="weui-check" id="num10_3" value="1">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>C. 钱可以自己支配，花钱的自由度高了</p>
                </div>
            </label>
     
            <label class="weui-cell weui-check__label" for="num10_4">
                <div class="weui-cell__hd">
                    <input type="checkbox" name="num10_4" class="weui-check" id="num10_4" value="1">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>D. 耽误精力，影响学习</p>
                </div>
            </label>
       
            <label class="weui-cell weui-check__label" for="num10_5">
                <div class="weui-cell__hd">
                    <input type="checkbox" name="num10_5" class="weui-check" id="num10_5" value="1">
                    <i class="weui-icon-checked"></i>
                </div>
                <div class="weui-cell__bd">
                    <p>E. 很辛苦，让我觉得很累</p>
                </div>
            </label>

        </div>
</div>
<div class="col-md-1"></div>
</div>


            <div class="weui-btn weui-btn_primary" href="javascript:" id="submit">提交</div>
        </div>




<div class="page__bd page__bd_spacing">
        <div class="weui-footer">
            <p class="weui-footer__links">
            </p>
            <p class="weui-footer__text">Copyright © 2008-2016 工大问答助手组</p>
        </div>
    </div>



<center><img src="wow.jpg"/></center>
</div>
<script type="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
	<script src ="zepto.min.js"></script>
	<script src ="example.js"></script>
	<script src ="bootstrap/dist/js/bootstrap.min.js"></script>

<script>
    $(document).ready(function(){
        $("#submit").click(function(){
            swal({
            title: "提交信息",
            text: "将提交消息",
            imageUrl:"imgs/thanks.jpg",
                    //type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#51CC00",
            confirmButtonText: "是 的",
            cancelButtonText: "稍 等",
            closeOnConfirm: false,
            closeOnCancel: false },
        function(isConfirm){
            if (isConfirm) {
                var title_result = "成功";
                var status_result = "success";
                var detail_result = "成功的提交了信息";
                $.post("/href",
                    {
                        age:$("input[name='age']").val(),
                        num1_1:$("input[name='num1_1']:checked").val(),
                        num1_2:$("input[name='num1_2']:checked").val(),
                        num1_3:$("input[name='num1_3']:checked").val(),
                        num1_4:$("input[name='num1_4']:checked").val(),
                        num1_5:$("input[name='num1_5']:checked").val(),
                        num2:$("input[name='num2']:checked").val(),
                        num3:$("input[name='num3']:checked").val(),
                        num4:$("input[name='num4']:checked").val(),
                        num5:$("input[name='num5']:checked").val(),
                        num6:$("input[name='num6']:checked").val(),
                        num7:$("input[name='num7']:checked").val(),
                        num8:$("input[name='num8']:checked").val(),
                        num9:$("input[name='num9']:checked").val(),
                        num10_1:$("input[name='num10_1']:checked").val(),
                        num10_2:$("input[name='num10_2']:checked").val(),
                        num10_3:$("input[name='num10_3']:checked").val(),
                        num10_4:$("input[name='num10_4']:checked").val(),
                        num10_5:$("input[name='num10_5']:checked").val(),
                    },
                    function(data){
                        if(data.status!=0)
                        {
                            title_result = "失败";
                            status_result = "error";
                            detail_result = "您可能有单选没选or年龄没写";
                        }swal(title_result, detail_result, status_result);
                    });


            } else {
                swal("未提交", "您选择了稍等", "error");   }
        });
        });
    });
</script>
</body>
</html>
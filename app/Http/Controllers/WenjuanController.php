<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Wenjuanone;

class WenjuanController extends Controller {


	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		return view('demo');
	}

    public function check(Request $request)
    {
        $model = new Wenjuanone;
        if(!empty($request->input('keyword')))
        {
            $keyword = $request->input('keyword');
            $searcher = $model::where('age','=',$keyword)->get();
            return view('store_user',["data" => $searcher]);
        }
        $result = $model::all();
        return view("check",["data" => $result]);

    }

	public function getMessage(Request $request)
    {
        try {
            $model = new Wenjuanone;
            $model->age = $request->input("age");
            if(!$request->input("num1_1"))$model->num1_1=0;
            else $model->num1_1 = $request->input("num1_1");
            if(!$request->input("num1_2"))$model->num1_2=0;
            else $model->num1_2 = $request->input("num1_2");
            if(!$request->input("num1_3"))$model->num1_3=0;
            else $model->num1_3 = $request->input("num1_3");
            if(!$request->input("num1_4"))$model->num1_4=0;
            else $model->num1_4 = $request->input("num1_4");
            if(!$request->input("num1_5"))$model->num1_5=0;
            else $model->num1_5 = $request->input("num1_5");

            $model->num2 = $request->input("num2");
            $model->num3 = $request->input("num3");
            $model->num4 = $request->input("num4");
            $model->num5 = $request->input("num5");
            $model->num6 = $request->input("num6");
            $model->num7 = $request->input("num7");
            $model->num8 = $request->input("num8");
            $model->num9 = $request->input("num9");

            if(!$request->input("num10_1"))$model->num10_1=0;
            else $model->num10_1 = $request->input("num10_1");
            if(!$request->input("num10_2"))$model->num10_2=0;
            else $model->num10_2 = $request->input("num10_2");
            if(!$request->input("num10_3"))$model->num10_3=0;
            else $model->num10_3 = $request->input("num10_3");
            if(!$request->input("num10_4"))$model->num10_4=0;
            else $model->num10_4 = $request->input("num10_4");
            if(!$request->input("num10_5"))$model->num10_5=0;
            else $model->num10_5 = $request->input("num10_5");

            if($model->age==0||$model->num2==0||$model->num3==0||$model->num4==0||$model->num5==0||$model->num6==0||$model->num7==0||$model->num8==0||$model->num9==0)
            {
                return response()->json(['status' =>  '1']);
            }
            if($model->num1_1==0 && $model->num1_2==0 && $model->num1_3==0 && $model->num1_4==0 && $model->num1_5==0)
            {
                return response()->json(['status' =>  '1']);
            }
            if($model->num10_1==0 && $model->num10_2==0 && $model->num10_3==0 && $model->num10_4==0 && $model->num10_5==0)
            {
                return response()->json(['status' =>  '1']);
            }
            $model->save();
        }
        catch(\Illuminate\Database\QueryException $e)
        {
            return response()->json(['status' =>  $e->getCode()]);
        }
        return response()->json(['status' => '0']);

    }



}

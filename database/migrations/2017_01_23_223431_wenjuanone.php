<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Wenjuanone extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('wenjuanone', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('num1_1');
            $table->integer('num1_2');
            $table->integer('num1_3');
            $table->integer('num1_4');
            $table->integer('num1_5');
            $table->integer('num2');
            $table->integer('num3');
            $table->integer('num4');
            $table->integer('num5');
            $table->integer('num6');
            $table->integer('num7');
            $table->integer('num8');
            $table->integer('num9');
            $table->integer('num10_1');
            $table->integer('num10_2');
            $table->integer('num10_3');
            $table->integer('num10_4');
            $table->integer('num10_5');

            $table->timestamps();



        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('wenjuanone');
	}

}
